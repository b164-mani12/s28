
//Javascript is by default is synchronous meaning that only one statement run/executed at a time
console.log("Hello s28");
console.log("Goodbye");

for(let i = 0; i<=1000; i++){
	console.log(i);
}

//blocking, is just a slow process of code.
console.log("hello world");


//Asynchronous Javascript
function printMe(){
	console.log("print me");
}

function test(){
	console.log("test");
}

setTimeout(printMe, 5000);
test();

//fetch is a method in JS, which allows us to send a request to an API and process its response

//fetch(url, {options}).then(response => response.json()).then(data => {console.log(data)});

//url = > the url resource/routes from the Server
//optional objects = > it contains additional information about our request such as method, the body and the headers.

console.log(fetch(`https://jsonplaceholder.typicode.com/posts`));

//retrieves all posts (GET)

fetch(`https://jsonplaceholder.typicode.com/posts`, {
	method: `GET`
})
.then(response => response.json())
.then(data =>{
	console.log(data)
});

//.then method captures the response object and returns another "promise" which will eventually be resolved or rejected


//"async" and "await" keywords

async function fetchData(){
	let result = await fetch(`https://jsonplaceholder.typicode.com/posts`)
	console.log(result);

	console.log(typeof result);

	console.log(result.body);

	let json = await result.json();

	console.log(json);
}

fetchData();

//creating a post
//create a new post following the REST API (create, post)
fetch(`https://jsonplaceholder.typicode.com/posts`, {
	method: `POST`,
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'New post',
		body: 'Hello World',
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json))

//updating a post
fetch(`https://jsonplaceholder.typicode.com/posts/1`, {
	method: `PUT`,
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: 'Updated post',
		body: 'Hello again',
		userId: 1
	})
})
.then(response => response.json())
.then(data => console.log(data))

//deleting a post
fetch(`https://jsonplaceholder.typicode.com/posts/1`, {
	method: `DELETE`
	})
.then(response => response.json())
.then(data => console.log(data))


//filtering posts
//the data can be filtered by sending the userId along with URL
//? symbol at the end of the URL indicates that parameters will be sent to the endpoint

fetch(`https://jsonplaceholder.typicode.com/posts?userId=1`)
.then(response => response.json())
.then(data => console.log(data))

//retrieving nested/related data

fetch(`https://jsonplaceholder.typicode.com/posts/1/comments`)
.then(response => response.json())
.then(data => console.log(data))
































