console.log("hello s28");

//3,
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'GET'
})
.then(response => response.json())
.then(data => console.log(data))


//4 & 5 getTitle
async function fetchData(){
	let result = await fetch(`https://jsonplaceholder.typicode.com/todos`)
	let data = await result.json();
	return data;

}

let title = fetchData();
//let titleList = title.map(function (value,label) {
//	return label;
//});

console.log(typeof fetchData());

//6
fetch('https://jsonplaceholder.typicode.com/todos?id=2', {
	method: 'GET'
})
.then(response => response.json())
.then(data => console.log(data))


//7 POST method
fetch(`https://jsonplaceholder.typicode.com/todos`, {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
		title: 'new entry to do task',
		completed: false
	})
})
.then(response => response.json())
.then(json => console.log(json))

//8 & 9 PUT method and updating a to do list
fetch(`https://jsonplaceholder.typicode.com/todos/1`, {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: "Updated post delectus aut autem",
		description: "household chores",
		status: "pending",
		dateCompleted: "pending",
		userId: 1
	})
})
.then(res => res.json())
.then(data => console.log(data))



//10 & 11 PATCH method
fetch(`https://jsonplaceholder.typicode.com/todos/1`, {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		dateCompleted: "complete"
	})
})
.then(res => res.json())
.then(data => console.log(data))



//12 DELETE method
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE'
})
.then(res => res.json())
.then(data => console.log(data))






























